#import RPi.GPIO as GPIO
import Adafruit_BBIO.GPIO as GPIO
import time
import math
import os
import glob
import logging
import subprocess


log = logging.getLogger('sousvide')

# GND = P9_1, P9_2
# 3v3 = P9_3, P9_4
#  5v = P9_5, P9_6
# s5v = P9_7, P9_8
#GPIO.setmode(GPIO.BCM)  # Use board pin numbering

#THERMOCOUPLE = "P9_22" # UART2_RXD
BURNER    = "P9_23" # GPIO1_17
GREEN_LED = "P9_24" # UART1_TXD
BLUE_LED  = "P9_21" # UART2_TXD
RED_LED   = "P9_26" # UART1_RXD

GREEN_VALUE = False
BLUE_VALUE = False
RED_VALUE = False


TOLERANCE = 0.2
GPIO.setup(GREEN_LED, GPIO.OUT)
GPIO.setup(RED_LED, GPIO.OUT)
GPIO.setup(BLUE_LED, GPIO.OUT)
GPIO.setup(BURNER, GPIO.OUT)

def toggle_green():
    global GREEN_VALUE
    GREEN_VALUE = not GREEN_VALUE
    GPIO.output(GREEN_LED, GREEN_VALUE)

def toggle_red():
    global RED_VALUE
    RED_VALUE = not RED_VALUE
    GPIO.output(RED_LED, RED_VALUE)

def toggle_blue():
    global BLUE_VALUE
    BLUE_VALUE = not BLUE_VALUE
    GPIO.output(BLUE_LED, BLUE_VALUE)

def green_off():
    global GREEN_VALUE
    GREEN_VALUE = False
    GPIO.output(GREEN_LED, GREEN_VALUE)

def red_off():
    global RED_VALUE
    RED_VALUE = False
    GPIO.output(RED_LED, RED_VALUE)

def blue_off():
    global BLUE_VALUE
    BLUE_VALUE = False
    GPIO.output(BLUE_LED, BLUE_VALUE)

if __name__ == '__main__':
    
    green_off()
    blue_off()
    red_off()


    toggle_green()
    time.sleep(1)
    toggle_red()
    time.sleep(1)
    toggle_blue()
    time.sleep(1)
    green_off()
    time.sleep(1)
    blue_off()
    time.sleep(1)
    red_off()
    time.sleep(1)

    GPIO.output(BURNER, True)
    time.sleep(1)
    GPIO.output(BURNER, False)


    GPIO.cleanup()