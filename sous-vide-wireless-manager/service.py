#!/bin/python
#
# service.py - The Wireless manager service program
#

import tornado.ioloop
import tornado.web
import time
from tornado.escape import json_decode, json_encode
from tornado import autoreload
from network import Network_Manager


class Wireless_Manager(obj):
    def __init__(self):
        self.network = Network_Manager()
        self.webserver = None
        self.retries = 0
        self._time_since_monitor = None
        self._time_since_adhoc = None
        self._uptime = time.time()
        self.mode = None

    def uptime(self):
        return time.time() - self._uptime

    def check_connection(self):
        return self.network.is_connected("http://www.google.com/")

    def connect(self, mode):
        self.network.enable_mode(mode)
        self.mode = mode

    def add_connection(self, data):
        #data from monitor or webserver
        self.write_netctl_file(data)
        webserver.disable()
        self.connect('managed')

    def write_netctl_file(self, data):
        netctl_file = '/etc/netctl/' + data.ssid

        output = 'Description=\'Automatically generated profile by wifi-menu\'\n'+
        'DHCPClient=dhclient\n'+
        'Interface=wlan0\n'+
        'Connection=wireless\n'+
        'Security='+ data.security_mode + '\n'+
        'ESSID=' + data.ssid + '\n'+
        'IP=dhcp\n'+
        'Key='+ data.key +'\n'

        # maybe detect if existing one and back it up ?
        f = open(netctl_file,'w')
        f.write(output) 
        f.close() 
 

    def run(self):
        if self.check_connection() or self.mode = 'ap':
            pass
        else:
            if self.retries < 3:
                self.connect('managed')
                self.retries = self.retries + 1
            else:
                self.retries = 0
                self.connect('ap')
                self._time_since_adhoc = time.time()
                self.webserver.enable()

service = Wireless_Manager()


class WirelessmanagerRequestHandler(tornado.web.RequestHandler):
    def _show(self):
        self.write({
            'mode': service.mode
        })

    def get(self, *args):
        self.write("<html>\
                    <head>\
                    <title>SousSet Configuration<title>\
                    </head>\
                    <body>\
                    <form action='/add_connection' method='post'>\
                    <p>\
                    <label for='ssid'>SSID: </label>\
                    <input type='text' id='ssid'><br>\
                    <label for='key'>Password: </label>\
                    <input type='text' id='key'><br>\
                    <label for='security_mode'>Security Mode: </label>\
                    <input type='text' id='security_mode'><br>\
                    </p>\
                    </form>\
                    </body>\
                    </html>")

    def post(self, *args):
        try:
            data = json_decode(self.request.body)
            #SSID = self.get_argument('ssid','')
            #key = self.get_argument('key','')
            #security_mode = self.get_argument('security_mode','')
            service.add_connection(data.get('connection', []))
        except Exception:
            raise tornado.web.HTTPError(
                400, 'Cannot decode JSON request: %s' % self.request.body)
        self._show()


application = tornado.web.Application([
    (r'/', WirelessmanagerRequestHandler),
    (r'/add_connection', WirelessmanagerRequestHandler),
])

if __name__ == '__main__':
    application.listen(3001)
    ioloop = tornado.ioloop.IOLoop.instance()

    periodic = tornado.ioloop.PeriodicCallback(service.run, 10000, ioloop)
    periodic.start()


    autoreload.start(ioloop)
    ioloop.start()
