#!/bin/py
#
# network.py

import logging
import subprocess
import time
import urllib

log = logging.getLogger('sousnetwork')


class Network_Manager(obj):
    def __init__(self):
        _state = None
        _last_change = time.time()

        if not self.is_connected('http://www.google.com'):
            self.enable_mode("managed")


    def state(self):
        return _state

    def time_since_last_change(self):
        return time.time() - _last_change

    def set_state(self, mode):
        _state = mode
        _last_change = time.time()

    def is_connected(self, refernce):
        try:
            urllib.request.urlopen(reference, timeout=5)
            return True
        except urllib.request.URLError:
            return False 

    def enable_mode(self, mode):
        if mode == "ap":
            #"ip link set wlan0 down"
            #"iw wlan0 set type ibss"
            #"ip link set wlan0 up"
            #"iw wlan0 ibss join sousset_device 2457" # channel 10
            try:
                log.debug('Enabling AP mode...')
                subprocess.check_call('/usr/bin/ip link set wlan0 down', shell=True)
                subprocess.check_call('./create_ap -n -g 192.168.1.1 wlan0 sousset', shell=True)
                log.debug('AP mode enabled.')
            except:
                print "AP: Unexpected error:", sys.exc_info()[0]

        if mode == "monitor":
            #"ip link set wlan0 down"
            #"iw wlan0 set type monitor"
            #"ip link set wlan0 up"
            try:
                log.debug('Enabling monitor mode...')
                subprocess.check_call('/usr/bin/ip link set wlan0 down', shell=True)
                subprocess.check_call('/usr/bin/iw wlan0 set type monitor', shell=True)
                subprocess.check_call('/usr/bin/ip link set wlan0 up', shell=True)
                log.debug('Monitor mode enabled.')
            except:
                print "Monitor: Unexpected error:", sys.exc_info()[0]

        if mode == "managed" or mode == "dhcp":
            #"ip link set wlan0 down"
            #"iw wlan0 set type managed"
            #"ip link set wlan0 up"
            ##"systemctl enable netctl-auto@wlan0.service"
            ##"netctl enable default" # default profile must exist
            # wait for 
            try:
                log.debug('Enabling managed mode...')
                subprocess.check_call('/usr/bin/ip link set wlan0 down', shell=True)
                subprocess.check_call('/usr/bin/iw wlan0 set type managed', shell=True)
                subprocess.check_call('/usr/bin/ip link set wlan0 up', shell=True)
                subprocess.check_call('/usr/bin/systemctl enable netctl-auto@wlan0.service', shell=True)
                subprocess.check_call('/usr/bin/systemctl start netctl-auto@wlan0.service', shell=True)
                log.debug('Managed mode enabled.')
            except:
                print "Managed: Unexpected error:", sys.exc_info()[0]
        self.set_state(mode)
