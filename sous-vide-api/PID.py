# PID library
# Example:
# 
from datetime import datetime

class PID(object):
    """
    PID Control
    """
    def __init__(   self, 
                    Input,
                    Output,
                    set_point,
                    Kp,
                    Ki,
                    Kd,
                    controller_direction = 1):
        self.start_time = datetime.now()
        self.output = Output
        self.input = Input
        self.last_input = self.input
        self.set_point = set_point
        self.kp = 0
        self.ki = 0
        self.kd = 0
        self.integral_term = 0
        self.automatic_control = False
        self.outMin = 0
        self.outMax = 255 # set to pwm limits of MCU
        self.sample_time = 100 # 100 ms
        self.controller_direction = 1

        self.set_output_limits(self.outMin,self.outMax) 
        self.set_controller_direction(controller_direction)
        self.set_tunings(Kp, Ki, Kd)
        self.last_time = self.millis()

    def millis(self):
        dt = datetime.now() - self.start_time
        ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
        return ms

    def set_output_limits(self, _min, _max):
        if(_min >= _max): return
        self.outMin = _min;
        self.outMax = _max;
        
        if self.automatic_control:
            if   self.output > self.outMax: 
                self.output = self.outMax
            elif self.output < self.outMin: 
                self.output = self.outMin

            if   self.integral_term > self.outMax: 
                self.integral_term = self.outMax
            elif self.integral_term < self.outMin:
                self.integral_term = self.outMin


    def set_automatic_control_mode(self, mode):
        new_auto = (mode is not False)
        if new_auto is not self.automatic_control: # detect manual -> auto change
            self.initialize()
        self.automatic_control = new_auto

    def initialize(self):
        self.integral_term = self.output
        self.last_input = self.input
        if   self.integral_term > self.outMax: self.integral_term = self.outMax
        elif self.integral_term < self.outMin: self.integral_term = self.outMin 

    def set_controller_direction(self, direction):
        if self.automatic_control and direction != self.controller_direction:
            self.kp = (0-self.kp)
            self.ki = (0-self.ki)
            self.kd = (0-self.kd)
        self.controller_direction = direction

    def set_tunings(self, Kp, Ki, Kd):
        if Kp < 0 or Ki< 0 or Kd<0: return
        sample_time_sec = self.sample_time/1000.0
        self.kp = Kp
        self.ki = Ki * sample_time_sec
        self.kd = Kd / sample_time_sec

        if self.controller_direction < 0 :
            self.kp = (0-self.kp)
            self.ki = (0-self.ki)
            self.kd = (0-self.kd)

    def set_sample_time(self, sample):
        if sample > 0 :
            ratio = sample/self.sample_time
            self.ki *= ratio
            self.kd /= ratio
            self.sample_time = sample



    def compute(self, Input):
        if not self.automatic_control: return
        self.input = Input
        now = self.millis()
        time_delta = now - self.last_time
        
        if time_delta >= self.sample_time :
            i = self.input
            error = self.set_point - i
            self.integral_term += self.ki * error

            if self.integral_term > self.outMax :
                self.integral_term = self.outMax
            elif self.integral_term < self.outMin :
                self.integral_term = self.outMin
            input_delta = i - self.last_input

            output = self.kp * error + self.integral_term - self.kd * input_delta
            
            if output > self.outMax:
                output = self.outMax
            elif output < self.outMin:
                output = self.outMin
            self.output = output
            #print 'output: %s, input: %s' % (self.output, i)


            self.last_input = i
            self.last_time = now
            #print("compute True")
            return self.output
        else: 
            #print("compute False")
            return False



    def get_kp(self): return self.display_kp
    def get_ki(self): return self.display_ki
    def get_kd(self): return self.display_kd

    def get_mode(self): return self.automatic_control
    def get_direction(self): return self.controller_direction
