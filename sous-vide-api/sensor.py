import time
import glob
import logging
import subprocess

DEVICES_DIR = '/sys/bus/w1/devices/'

log = logging.getLogger('soussensor')


class SousvideError(Exception):
    pass


def get_device():
    try:
        log.debug('Starting probe services.')
        subprocess.check_call('/sbin/modprobe w1-gpio', shell=True)
        subprocess.check_call('/sbin/modprobe w1-therm', shell=True)
        log.debug('Started probe services.')
    except subprocess.CalledProcessError, e:
        raise SousvideError('Cannot start probe services: %s' % e)
    try:
        device_folder = glob.glob('%s28*' % DEVICES_DIR)[0]
    except IndexError, e:
        raise SousvideError('Cannot find a device folder: %s' % e)
    return device_folder + '/w1_slave'


def read_temp_raw(device_file):
    try:
        log.debug('Reading sensor data.')
        with open(device_file, 'r') as f:
            return f.readlines()
    except IOError, e:
        raise SousvideError('Failed to read raw temperature data: %s' % e)


def read_temp():
    device_file = get_device()
    log.debug('Checking sensor data state.')
    while True:
        lines = read_temp_raw(device_file)
        try:
            if lines[0].strip()[-3:] == 'YES':
                break
        except IndexError:
            pass
        time.sleep(0.2)
    log.debug('Parsing sensor data.')
    try:
        equals_pos = lines[1].find('t=')
    except IndexError:
        raise SousvideError('Failed to parse the sensor data: %s' % e)
    if equals_pos != -1:
        try:
            temp_string = lines[1][(equals_pos + 2):]
        except IndexError:
            raise SousvideError('Failed to get the temperature string: %s' % e)
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    try:
        temperature = read_temp()
        log.info('Temperature: %s', temperature)
    except SousvideError, e:
        log.error(e)
