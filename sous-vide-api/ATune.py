import time

class ATune(object):
	def __init__(self):
		self.input = None
		self.output = None
		self.pid = None
		self.control_type = 0 # default to PI
		self.noise_band = 0.5
		self.running = False
		self.o_step = 10
		self.peak_count = None
		self.last_inputs = []
		self.sample_time = None
		self.n_lookback = None
		self.setpoint = None

		self.set_lookback_sec(10);
		self.last_time = time.time()
	
	def cancel(self):
		self.running = False

	def set_pid(self, PID):
		self.pid = PID

	def runtime(self):
		#evaled = False
		
		i = self.pid.get_current_temperature()#self.input
		if len(self.last_inputs) >= self.n_lookback:
			self.last_inputs.pop();
		self.last_inputs.insert(0,i)

		if(self.peak_count > 9 and self.running):
			self.running = False
			self.finish()
			return True

		now  = time.time()

		if (now - self.last_time) < self.sample_time:
			return False

		self.last_time = now
		self.output = self.pid.get_output_val()
		#evaled = True
		if not self.running :
			self.peak_type = 0
			self.peak_count = 0
			self.recent_change = False
			self.abs_max = i
			self.abs_min = i
			self.setpoint = i
			self.running = True
			self.output_start = self.output
			self.output = self.output_start + self.o_step
		else:
			if i > self.abs_max :
				self.abs_max = i
			if i < self.abs_min :
				self.abs_min = i

		if i > self.pid.get_setpoint() + self.noise_band :
			self.output = self.output_start - self.o_step
		elif i < self.pid.get_setpoint() - self.noise_band :
			self.output = self.output_start + self.o_step


		is_max = True
		is_min = True


		for index in range(self.n_lookback - 1, -1, -1):
			if is_max : is_max = i > self.last_inputs[index]
			if is_min : is_min = i < self.last_inputs[index]

		if self.n_lookback < 9 :
			return False

		if is_max:
			if self.peak_type == 0 :
				self.peak_type = 1
			if self.peak_type == -1 :
				self.peak_type = 1
				self.recent_change = True
				self.peak2 = self.peak1
			self.peak1 = now
			self.peaks[self.peak_count] = i
		elif is_min:
			if self.peak_type == 0 :
				self.peak_type = -1
			if self.peak_type == 1 :
				self.peak_type = -1
				self.recent_change = True
				self.peak_count += 1
			if self.peak_count < 10 :
				self.peaks[self.peak_count] = i


		if self.recent_change and self.peak_count > 2 :
			avg_seperation = (abs(self.peaks[self.peak_count-1]-self.peaks[self.peak_count-2])+abs(self.peaks[self.peak_count-2]-self.peaks[self.peak_count-3]))/2
			if avg_seperation < 0.05*(self.abs_max-self.abs_min):
				self.finish()
				self.running = False
				return True

		self.running = False
		return False

	def finish(self):
		self.output = self.output_start
		self.ku = 4*(2*self.o_step)/((self.abs_max - self.abs_min)*3.14159)
		self.pu = (self.peak1 - self.peak2)/1000


	def get_kp(self):
		return {True:  0.6 * self.ku, 
				False: 0.4 * self.ku}[self.control_type == 1]

	def get_ki(self):
		return {True:  1.2 * self.ku / self.pu, 
				False: 0.4 * self.ku / self.pu }[self.control_type == 1]

	def get_kd(self):
		return {True:  0.075 * self.ku * self.pu , 
				False: 0}[self.control_type == 1]

	def set_output_step(self, step):
		self.o_step = step

	def get_output_step(self): 
		return self.o_step

	def set_control_type(self, t):
		self.control_type = t;

	def set_setpoint(self, s):
		self.setpoint = s;

	def get_control_type(self): 
		return self.control_type

	def set_noise_band(self, band):
		self.noise_band = band

	def get_noise_band(self): 
		return self.noise_band

	def get_lookback_sec(self):
		return self.n_lookback * self.sample_time / 1000

	def set_lookback_sec(self, val):
		if val <1 :
			val =1

		if val<25:
			self.n_lookback = val
			self.sample_time = 10 * 2
		else:
			self.n_lookback = 100
			self.sample_time = val*10


