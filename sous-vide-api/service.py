import zmq
from zmq.eventloop.zmqstream import ZMQStream
from zmq.eventloop import ioloop
ioloop.install()

import time
import random
import math
import logging
from collections import deque
import tornado.ioloop
import tornado.web
from tornado.escape import json_decode, json_encode
from tornado import autoreload
from tornado.websocket import WebSocketHandler


# GND = P9_1, P9_2
# 3v3 = P9_3, P9_4
#  5v = P9_5, P9_6
# s5v = P9_7, P9_8
#GPIO.setmode(GPIO.BCM)  # Use board pin numbering

#THERMOCOUPLE = "P9_22" # UART2_RXD
BURNER    = "P9_23" # GPIO1_17
GREEN_LED = "P9_24" # UART1_TXD
BLUE_LED  = "P9_21" # UART2_TXD
RED_LED   = "P9_26" # UART1_RXD

#GREEN_VALUE = False
#BLUE_VALUE = False
#RED_VALUE = False

try:
    #import RPi.GPIO as GPIO
    import Adafruit_BBIO.GPIO as GPIO
    PI = True
except ImportError:
    PI = False

if PI:
    #GPIO.setmode(GPIO.BCM)
    GPIO.setup(GREEN_LED, GPIO.OUT)
    GPIO.setup(RED_LED, GPIO.OUT)
    GPIO.setup(BURNER, GPIO.OUT)

from sensor import read_temp

from PID import PID
#from ATune import ATune


log = logging.getLogger('sousservice')
ctx = zmq.Context()

s = ctx.socket(zmq.REQ)
s.connect('tcp://192.168.1.45:5069')
stream = ZMQStream(s)

"""
class ATuneService(object):
    def __init__(self):
        self.step = 500
        self.noise = 1
        self.lookback = 10
        self.tuning = False
        self.working = False
        self.ready = False
        self.ATune = ATune()
        self.kp = 0
        self.ki = 0
        self.kd = 0

    def start(self):
        self.ATune.set_noise_band(self.noise)
        self.ATune.set_output_step(self.step)
        self.ATune.set_lookback_sec(self.lookback)
        self.tuning = True

    def finish(self):
        self.tuning = False
        self.kp = self.ATune.get_kp()
        self.ki = self.ATune.get_ki()
        self.kd = self.ATune.get_kd()
        self.ready = True # want this to really be a callback

    def get_pid_tunings(self):
        return [self.kp,self.ki,self.kd]

    def get_available_pid(self):
        self.ready = False
        return self.get_pid_tunings()

    def is_ready(self):
        return self.ready

    def set_pid(self, PID):
        self.ATune.set_pid(PID)

    def run(self):
        if self.tuning is True and self.working is False:
            self.working = True
            if self.ATune.runtime():
                self.finish()
                self.working = False
            else:
                self.working = False

        else:
            pass
"""

class PIDService(object):
    def __init__(self):
        self.input = 0
        self.output = 0
        self.setpoint = 57
        self.kp = 2.0
        self.ki = 0.3
        self.kd = 1.0
        self.direction = 1
        self.window_size = 5 #sec
        self.window_start_time = time.time()
        self.current_temperature = 0
        self.PID = PID( self.input,
                        self.output,
                        self.setpoint,
                        self.kp,
                        self.ki,
                        self.kd,
                        self.direction)
        self.PID.set_tunings(self.kp,self.ki,self.kd)
        self.PID.set_sample_time(200)
        self.PID.set_output_limits(0, self.window_size);

    def compute(self):
        self.input = read_temp()[0]
        #self.input = self.read_temperature()
        self.current_temperature = self.input
        self.output = self.PID.compute(self.input)

    def get_current_temperature(self):
        return self.current_temperature

    def update(self, setpoint):
        self.PID = None
        self.setpoint = setpoint
        self.PID = PID( self.input,
                        self.output,
                        self.setpoint,
                        self.kp,
                        self.ki,
                        self.kd,
                        self.direction)
        print 'input: %s, output: %s, setpoint: %s, kp: %s, ki: %s, kd: %s, direction: %s' % (self.input,
                        self.output,
                        self.setpoint,
                        self.kp,
                        self.ki,
                        self.kd,
                        self.direction)
        self.set_automatic()

    def update_pid(self, Kp, Ki, Kd):
        self.kp = Kp
        self.ki = Ki
        self.kd = Kd
        self.PID = PID( self.input,
                        self.output,
                        self.setpoint,
                        self.kp,
                        self.ki,
                        self.kd,
                        self.direction)
        self.set_automatic()

    def set_manual(self):
        self.PID.set_automatic_control_mode(False);

    def set_automatic(self):
        self.PID.set_automatic_control_mode(True);

    def read_temperature(self):
        mantissa = math.sin(time.time())/10.0

        if self.get_output():
            return max(0, self.current_temperature + random.choice(range(4))*0.125 + mantissa)
        else:
            return max(0, self.current_temperature - random.choice(range(3))*0.0675 + mantissa)

    def get_output(self):
        now = time.time();
        #print 'output: %s, now: %s, wst: %s, ws: %s' % (self.output,now,self.window_start_time,self.window_size)

        if now - self.window_start_time > self.window_size :
            self.window_start_time += self.window_size
        if self.output > now - self.window_start_time :
            return True
        else:
            return False

    def get_output_val(self):
        return self.output

    def get_setpoint(self):
        return self.setpoint


class SousvideService(object):
    def __init__(self):
        self.start_time = None
        self.end_time = None
        self.pid = None
        self.atune = None
        self.phases = []
        self.current_phase_index = 0
        self.current_temperature = 0
        self.moving_average = 0

        self._tolerance = 0.1
        self._power = False
        self._hold_temp = False
        self._queue_size = 5
        self._queue = deque(maxlen=self._queue_size)

        self._green = False
        self._red = False

        self.transitions = {
            ('preparing', 'waiting'): ('start_phase',),
            ('cooking', 'waiting'): ('next_phase',),
        }
        self.state = 'waiting'

    def _transition(self, state):
        prev_state = self.state
        self.state = state

        self.red_off()
        self.green_off()


        if state != 'waiting':
            self.pid.update(self.target_temperature)
            #self.atune.start()
        else:
            if not self._hold_temp: #turn PID control off if not holding
                self.pid.set_manual()
            else:
                pass
        print '%s -> %s' % (prev_state, state)
        try:
            args = self.transitions[(prev_state, state)]
            callback = getattr(self, args[0])
            callback(*args[1:])
        except (AttributeError, KeyError):
            pass

    def set_pid(self, PID):
        self.pid = PID

    def set_tuner(self, ATUNE):
        self.atune = ATUNE

    def reset(self):
        self.start_time = None
        self.end_time = None
        self.phases = []
        self.current_phase_index = 0
        self.current_temperature = 0
        self.moving_average = 0

        self._tolerance = 0.5
        self._power = False
        self._queue_size = 5
        self._queue = deque(maxlen=self._queue_size)

        self.state = 'waiting'
        self.green_off()
        self.red_off()
        self.output_off(BURNER)

    def toggle_green(self):
        self._green = not self._green
        if PI:
            GPIO.output(GREEN_LED, self._green)

    def toggle_red(self):
        self._red = not self._red
        if PI:
            GPIO.output(RED_LED, self._red)

    def green_off(self):
        self._green = False
        if PI:
            GPIO.output(GREEN_LED, self._green)

    def red_off(self):
        self._red = False
        if PI:
            GPIO.output(RED_LED, self._red)

    def green_on(self):
        self._green = True
        if PI:
            GPIO.output(GREEN_LED, self._green)

    def red_on(self):
        self._red = True
        if PI:
            GPIO.output(RED_LED, self._red)

    def output_on(self, channel):
        if PI:
            GPIO.output(channel, GPIO.HIGH)

    def output_off(self, channel):
        if PI:
            GPIO.output(channel, GPIO.LOW)

    @property
    def current_phase(self):
        try:
            return self.phases[self.current_phase_index]
        except IndexError:
            pass

    @property
    def is_last_phase(self):
        try:
            self.phases[self.current_phase_index + 1]
            return False
        except IndexError:
            return True

    @property
    def target_temperature(self):
        if self.current_phase is not None:
            return self.current_phase[0]

    @property
    def current_phase_duration(self):
        if self.current_phase is not None:
            return self.current_phase[1] / 1000

    def start_phase(self):
        self._transition('cooking')
        self.red_off()
        self.toggle_green()
        self.start_time = time.time()
        self.end_time = self.start_time + self.current_phase_duration

    def next_phase(self):
        try:
            phase = self.phases[self.current_phase_index + 1]
            self.current_phase_index += 1
            self._transition('preparing')
            return phase
        except IndexError:
            pass

    def run(self):
        self.current_temperature = read_temp()[0]
        #self.current_temperature = self.pid.read_temperature()

        self._queue.append(self.current_temperature)
        self.moving_average = sum(self._queue) / self._queue_size

        """
        if(self.atune.is_ready()):
            tpid = self.atune.get_available_pid()
            self.pid.update_pid(tpid[0],tpid[1],tpid[2])
            print '\n'
            print '*** AUTOTUNE UPDATE ***'
            print 'Kp: %s, Ki: %s, Kd:%s'%(tpid[0],tpid[1],tpid[2])
            print '\n'
        """

        if self.state == 'preparing':
            self.toggle_green()
            self.toggle_red()


            if abs(self.target_temperature - self.moving_average) < self._tolerance:
                self._transition('waiting')

        elif ((self.state == 'cooking' and time.time() > self.end_time) or
              self.state == 'skip'):
            self._transition('waiting')
            if self.is_last_phase:
                self.green_on()
                self._hold_temp = True
            else:
                self.green_off()
            self.red_off()

        elif self.state == 'cooking':
            self.toggle_green()

        # PID control controls relay
        if self.pid.get_output():
            self.output_on(BURNER)
            self._power = True
        else:
            self.output_off(BURNER)
            self._power = False




        stat = 'time: %s, phase: %s, temp: %s, power: %s, state: %s, Kp: %s, Ki: %s, Kd: %s, output:%s' % (
            time.time(),
            self.current_phase,
            self.current_temperature,
            self._power,
            self.state,
            self.pid.kp,
            self.pid.ki,
            self.pid.kd,
            self.pid.get_output_val())

        s.send(json_encode({
            'time': time.time(), 'currentPhase': self.current_phase,
            'currentTemperature': self.current_temperature,
            'power': self._power, 'state': self.state,
            'kp': self.pid.kp, 'ki': self.pid.ki, 'kd': self.pid.kd,
            'output': self.pid.get_output_val()
        }))

        def blah(msg):
            print 'Got from the server: %s' % msg

        stream.on_recv(blah)

        print stat

    def update_phase(self, phase_id, data):
        self.phases[phase_id] = [data.get('temperature'),
                                 data.get('duration')]

    def add_phases(self, phases):
        self.reset()
        self.phases = [[phase.get('temperature'),
                        phase.get('duration')] for phase in phases]
        self._transition('preparing')

    def delete_phase(self, phase_index):
        if phase_index > self.current_phase_index:
            del self.phases[phase_index]

    def skip_to_phase(self, phase_index):
        self.current_phase_index = phase_index
        self._transition('skip')

pid_service = PIDService()
#atune_service = ATuneService()
service = SousvideService()
service.set_pid(pid_service)
#atune_service.set_pid(pid_service)
#service.set_tuner(atune_service)


class SousvideRequestHandler(tornado.web.RequestHandler):
    def _show(self):
        self.write({
            'state': service.state,
            'startTime': service.start_time,
            'endTime': service.end_time,
            'currentTemperature': service.current_temperature,
            'movingAverage': service.moving_average,
            'currentPhaseId': service.current_phase_index,
            'phases': [{'id': index, 'temperature': phase[0],
                        'duration': phase[1]}
                       for index, phase in enumerate(service.phases)],
            'pid': [service.pid.kp,service.pid.ki,service.pid.kd]
        })

    def get(self, *args):
        if 'stop' in args:
            service.reset()
        self._show()

    def post(self, *args):
        if 'stop' in args:
            service.reset()
        else:
            try:
                data = json_decode(self.request.body)
                service.add_phases(data.get('phases', []))
            except Exception:
                raise tornado.web.HTTPError(
                    400, 'Cannot decode JSON request: %s' % self.request.body)
        self._show()

    def put(self, phase_id):
        if phase_id is not None and phase_id < service.current_phase_index:
            raise tornado.web.HTTPError(400, 'Phase already executed.')
        try:
            data = json_decode(self.request.body)
        except Exception:
            raise tornado.web.HTTPError(
                400, 'Cannot decode JSON request: %s' % self.request.body)
        if phase_id is None or phase_id == '':

            phase_id = data.get('currentPhaseId')
            service.skip_to_phase(int(phase_id))

        else:
            try:
                service.update_phase(int(phase_id), data)
            except IndexError:
                raise tornado.web.HTTPError(404, 'Phase not found.')
        self._show()

    def delete(self, phase_id):
        if phase_id is None:
            raise tornado.web.HTTPError(400, 'Phase ID is required.')
        if phase_id <= service.current_phase_index:
            raise tornado.web.HTTPError(400, 'Phase already executed.')
        try:
            service.delete_phase(int(phase_id))
        except IndexError:
            raise tornado.web.HTTPError(404, 'Phase not found.')
        self._show()


class EchoWebSocket(WebSocketHandler):
    def open(self):
        print "WebSocket opened"
        self.periodic = tornado.ioloop.PeriodicCallback(self.send_update, 1000)
        self.periodic.start()

    def on_message(self, message):
        print "WebSocket message: %s" % message

    def on_close(self):
        print "WebSocket closed"
        self.periodic.stop()

    def send_update(self):
        self.write_message(json_encode({
            'temp': service.current_temperature,
            'ma': service.moving_average,
            'target': service.target_temperature,
            'pid': [service.pid.kp,service.pid.ki,service.pid.kd]
        }))


application = tornado.web.Application([
    (r'/([0-9]*)', SousvideRequestHandler),
    (r'/(stop)', SousvideRequestHandler),
    (r'/ws', EchoWebSocket),
])


if __name__ == '__main__':
    application.listen(8888)
    ioloop = tornado.ioloop.IOLoop.instance()

    periodic = tornado.ioloop.PeriodicCallback(service.run, 1000, ioloop)
    periodic.start()

    periodic2 = tornado.ioloop.PeriodicCallback(pid_service.compute, 500, ioloop)
    periodic2.start()

    #periodic3 = tornado.ioloop.PeriodicCallback(atune_service.run, 1000, ioloop)
    #periodic3.start()

    autoreload.start(ioloop)
    ioloop.start()
